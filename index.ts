import express from "express";
import bodyParser from "body-parser";
import * as line from "@line/bot-sdk";
import PQueue from "p-queue";
import "dotenv/config";

const config = {
  channelAccessToken: process.env.CHANNEL_ACCESS_TOKEN as string,
  channelSecret: process.env.CHANNEL_SECRET as string,
};
const adminId = process.env.ADMIN_ID as string;

let start = false;
let number = 0;
let approvedNnums = 0;
let candidatesNums = 0;
let allUser: string[] = [];

const client = new line.Client(config);
const app = express();
const queue = new PQueue({ concurrency: 1 });

app.use("/webhook", line.middleware(config));
app.use(bodyParser.json());

app.post("/webhook", (req, res) => {
  Promise.all(req.body.events.map(handleEvent))
    .then(() => res.sendStatus(200))
    .catch((e) => {
      console.error(e);
      res.sendStatus(500).end();
    });
});

const handleEvent = (event: line.WebhookEvent) => {
  return queue.add(() => processEvent(event));
};

async function processEvent(event: line.WebhookEvent) {
  console.log("now process", event);

  if (event.type === "message" && event.message.type === "text") {
    return handleTextMessage(event);
  }

  return Promise.resolve();
}

function handleTextMessage(event: line.MessageEvent) {
  if (event.message.type === "text") {
    if (
      event.message.text.includes("start") &&
      event.source.userId === adminId
    ) {
      const arr = event.message.text.split(" ");
      console.log(arr);
      if (!arr[1] && !arr[2])
        return client.replyMessage(event.replyToken, {
          type: "text",
          text: "參數錯誤",
        });

      approvedNnums = parseInt(arr[1]);
      candidatesNums = parseInt(arr[2]);
      start = true;
      allUser = [];
      return client.replyMessage(event.replyToken, {
        type: "text",
        text: "開始報名",
      });
    } else if (
      event.message.text === "end" &&
      start === true &&
      event.source.userId === adminId
    ) {
      start = false;
      number = 0;
      return client.replyMessage(event.replyToken, {
        type: "text",
        text: "結束報名",
      });
    } else if (start === true) {
      // if (!event.message.text.includes("報名")) return Promise.resolve();
      // const name = event.message.text.split(" ")[1];
      // if (!name) return Promise.resolve();
      const name = event.message.text;
      number++;
      if (number > approvedNnums + candidatesNums) {
        start = false;
        number = 0;
        approvedNnums = 0;
        candidatesNums = 0;
        return client.replyMessage(event.replyToken, {
          type: "text",
          text: "人數已達上限，結束報名",
        });
      }

      const replyText = `姓名: ${name}，報名: ${
        number > approvedNnums ? "備取" : "正取"
      } ${number > approvedNnums ? number - approvedNnums : number}`;
      allUser.push(replyText);
      const replyMessage: line.TextMessage = {
        type: "text",
        text: replyText,
      };
      return client.replyMessage(event.replyToken, replyMessage);
    } else if (
      event.message.text === "userlist" &&
      event.source.userId === adminId
    ) {
      return client.replyMessage(event.replyToken, {
        type: "text",
        text: allUser.join("\n"),
      });
    } else {
      return Promise.resolve();
    }
  }
}

app.listen(3000, () => {
  console.log("Line Bot is running on port 3000");
});
